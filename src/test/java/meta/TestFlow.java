package meta;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

/**
 * Класс родитель для основных тестов (но не для юнит-тестов!), содержит все нужные пути, настройки логера,
 * обеспечивает начало и конец сьюта, записи в лог.
 */
public class TestFlow {
    // Путь к конфигу логера, там задается как и куда записывать сообщения
    private final String PATH_TO_LOGGER_CONFIG = System.getProperty("user.dir") + "\\config.xml";
    private long beginTime;
    public static Logger logger = LoggerFactory.getLogger("main_log");
    // Флаг пустого прогона тестов, в случае true тесты просто пишут полученные тестовые данные и завершаются успешно. Нужно для отладки и проверок
    protected boolean fakeRun = false;

    @BeforeSuite
    public void startSuite() {
        initLogger();
        logger.info("SUITE STARTED!");
        beginTime = System.currentTimeMillis();
    }

    @AfterSuite
    public void stopSuite() {
        logger.info("SUITE FINISHED!");
        if (!fakeRun) {
            // закрываем коннекты, убиваем драйверы
        }
        long totalTimeInSeconds = (System.currentTimeMillis() - beginTime) / 1000;
        int minutes = (int) totalTimeInSeconds / 60;
        int seconds = (int) totalTimeInSeconds - (minutes * 60);
        logger.info("Total time: " + minutes + " min " + seconds + " sec.");
    }

    /**
     * Пытаемся инициализировать логер с помощью конфига
     */
    private void initLogger() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        try {
            JoranConfigurator configurator = new JoranConfigurator();
            configurator.setContext(context);
            context.reset();
            configurator.doConfigure(PATH_TO_LOGGER_CONFIG);
        } catch (JoranException je) {
            System.err.println("Cant initialize logger! \n" + je.getMessage());
            je.printStackTrace();
        }
    }
}
